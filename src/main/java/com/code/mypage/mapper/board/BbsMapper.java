package com.code.mypage.mapper.board;

import com.code.mypage.vo.board.BbsDataVO;
import com.code.mypage.vo.board.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BbsMapper {

    //가져올 리스트의 전체 개수를 구한다.
    int getTotalCount(SearchVO searchVO) throws Exception;
    //한페이지당 보여줄 데이터리스트를 가져온다.
    List<BbsDataVO> getBbsList(SearchVO searchVO) throws Exception;
}
