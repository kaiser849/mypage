package com.code.mypage.service.login;

import com.code.mypage.mapper.login.LoginMapper;
import com.code.mypage.vo.login.UserInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class LoginService {

    private BCryptPasswordEncoder passwordEncoder;
    private LoginMapper loginMapper;

    @Autowired
    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setLoginMapper(LoginMapper loginMapper) {
        this.loginMapper = loginMapper;
    }


    public int isLogin(Map<String, Object> param) throws Exception {

        UserInfoVO userInfo = loginMapper.getUserInfo(param);
        int result  = 0;
        if(userInfo != null) {
            //비교할때는  (암호화 안된 패스워드, 저장된(암호화된)  패스워드)
            boolean  isMatched = passwordEncoder.matches(param.get("userPw").toString(), userInfo.getUserPw());

            if(isMatched) {
                result = 1;  // 비밀번호가 맞다면 1
            }else {
                result = -1;  //비밀번호가 틀리면 -1
            }
        }else {
            result = -1;  // 아이디가 틀리면 -1
        }

        return result;
    }

    public int  insertUserInfo(UserInfoVO userInfo) throws  Exception {
        int result  = 0;

        String securedPass = passwordEncoder.encode(userInfo.getUserPw());
        userInfo.setUserPw(securedPass); // bcrypt 를 이용한 패스워드 암호화
        result = loginMapper.insertUserInfo(userInfo);

        return result;
    }

}
