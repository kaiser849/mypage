package com.code.mypage.service.board;

import com.code.mypage.mapper.board.BbsMapper;
import com.code.mypage.vo.board.BbsDataVO;
import com.code.mypage.vo.board.ParamVO;
import com.code.mypage.vo.board.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BbsService {

    private BbsMapper bbsMapper;

    @Autowired
    public void setBbsMapper(BbsMapper bbsMapper) {
        this.bbsMapper = bbsMapper;
    }

    //가져올 리스트의 전체 개수를 구한다.
    public int getTotalCount(SearchVO searchVO) throws Exception{
        return bbsMapper.getTotalCount(searchVO);
    }
    //한페이지당 보여줄 데이터리스트를 가져온다.
    public List<BbsDataVO> getBbsList(SearchVO searchVO) throws Exception{
        return bbsMapper.getBbsList(searchVO);
    }

    public Map<String, Object> insertBBS(ParamVO vo) throws  Exception {
        return null;
    }
}
