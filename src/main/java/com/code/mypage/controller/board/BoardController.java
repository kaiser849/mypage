package com.code.mypage.controller.board;

import com.code.mypage.service.board.BbsService;
import com.code.mypage.vo.board.BbsDataVO;
import com.code.mypage.vo.board.PagingVO;
import com.code.mypage.vo.board.ParamVO;
import com.code.mypage.vo.board.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/bbs")
public class BoardController {

    @Value("${server.file.upload.folder}")
    private String filePath;

    private BbsService service;

    @Autowired
    public void setBbsService(BbsService service) {
        this.service = service;
    }

    @RequestMapping(value = "/list")
    public ModelAndView boardList(@ModelAttribute SearchVO searchVO) {

        ModelAndView view = new  ModelAndView();
        view.setViewName("views/board/bbs_list");

        List<BbsDataVO> bbsList = null;

        try {

            int totalCount = service.getTotalCount(searchVO);
            //페이징 처리 객체를 선언한다.
            PagingVO pageVO = new PagingVO();
            //구해온 전체 리스트 개수를 페이지처리 객체에 넣어준다.
            pageVO.setTotalCount(totalCount);
            //클라이언트에서 넘어온 이동할 페이지정보를 페이징처리 객체에 넣어준다.
            pageVO.setCurrentPage(searchVO.getCurrentPage());

            searchVO.setStart(pageVO.getStartRow()); //시작위치
            searchVO.setEnd(pageVO.getCountPerPage()); // 한페이지에 보여줘야할 범위

            //페이지에 뿌릴 데이터를 가져온다.
            bbsList = service.getBbsList(searchVO);

            view.addObject("currentPage", searchVO.getCurrentPage());
            view.addObject("pageHtml", pageVO.getPager());
            view.addObject("bbsList", bbsList);

            //리스트가 있을 경우는 전체카운트를 보내고 없으면 그냥 0으로 준다.
            if(bbsList != null) {
                view.addObject("listSize", totalCount);
            }else{
                view.addObject("listSize", 0);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    @RequestMapping(value = "/add/view")
    public ModelAndView addFormView() {

        ModelAndView view = new  ModelAndView();
        view.setViewName("views/board/add_board");

        return view;
    }


    @RequestMapping(value = "/add/proc")
    @ResponseBody
    public Map<String, Object> addFormView(@ModelAttribute ParamVO paramVO) {

        Map<String, Object> resultMap = new HashMap<String, Object>();

        return resultMap;
    }

}
