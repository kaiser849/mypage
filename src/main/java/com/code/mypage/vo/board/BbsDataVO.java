package com.code.mypage.vo.board;

import lombok.Data;

@Data
public class BbsDataVO {

    private int bbsId;
    private String bbsTitle;
    private String bbsContents;
    private String bbsAuthor;
    private String bbaOriginFile;
    private String bbsStoreFile;
    private String bbsPath;
    private int readCount;
    private String regDate;
    private String modeDate;
}
