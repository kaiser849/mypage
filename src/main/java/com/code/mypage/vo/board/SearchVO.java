package com.code.mypage.vo.board;

import lombok.Data;

@Data
public class SearchVO {
    private int start;
    private int end;
    private int currentPage;
}
